module.exports = function(app, mongoose) {
    var userSchema = new mongoose.Schema({
      "full_name"    : { type : String,trim:true },
      "user_name"    : { type : String,trim:true },
      "password"     : { type : String },
      "email_id"     : { type : String,unique:true},
      "created_date" : { type : Date, default:new Date() },
      "phone_number" : { type : String,unique:true },
      "room_id"      : { type : String},
      "hash"         : String, 
      "salt"         : String 
    });

    userSchema.methods.setPassword = function(password) { 
     
      // Creating a unique salt for a particular user 
         this.salt = crypto.randomBytes(16).toString('hex'); 
       
         // Hashing user's salt and password with 1000 iterations,
         this.hash = crypto.pbkdf2Sync(password, this.salt,  
         1000, 64, `sha512`).toString(`hex`); 
     }; 

     userSchema.methods.validPassword = function(password) { 
      var hash = crypto.pbkdf2Sync(password,  
      this.salt, 1000, 64, `sha512`).toString(`hex`); 
      return this.hash === hash; 
  }; 

    app.db.model('user', userSchema);
  };
  