'use strict';

module.exports = function(app, mongoose) {
  require('./schema/User')(app, mongoose);
};
