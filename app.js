'use strict';
//dependencies
var express     = require('express'),
    bodyParser  = require('body-parser'),
    mongoose    = require('mongoose'),
    config      = require('./config')(),
    jwt = require('jsonwebtoken'),
    path        = require('path'),
    ss = require('socket.io-stream');
var app = express();
var http = require('http').createServer(app);
var io = require('socket.io')(http).listen(http);
//setup mongoose
app.db = mongoose.createConnection(config.mongodb.uri,config.mongodb.options);
app.db.on('error', function(err){
  console.log(err)
  process.exit(1);
}) 
app.db.once('open', function () {
    //and... we have a data store
    app.use(require('serve-static')(path.join(__dirname, 'public')));
    app.use('/public', (req, res, next)=> { console.log(req); next()}, express.static('public'))

    require('./models')(app, mongoose);
    //settings
    app.set('port', config.port);
    app.use(bodyParser.json({limit: '16mb'}));
    app.use(bodyParser.urlencoded({ limit: '16mb', extended: true }));
    app.all('/*', function (request, response, next) {
      response.header('Access-Control-Allow-Origin', '*');
      response.header('Access-Control-Allow-Headers', '*');
      response.header('Access-Control-Expose-Headers', '*');
      response.header('Access-Control-Allow-Methods', 'GET, PUT, POST, DELETE, OPTIONS, HEAD');
      response.header('Access-Control-Allow-Credentials', 'true');
      next();
  });
 
    //setup routes


/*
* room id is generated from client side,which another user should use to join
*/    

    app.post('/room',(req,res)=>{
       //validate jwt token(not implimenting now)
      //we can get user details from jwt decrypted token(but now implimented by getting user email from client side)
      // client = io.connect('http://localhost:3000');
      // client.emit('create',req.body.room_name);
      //above lines should be from client side)
      //update roomid of user who created to start chat
      let room =req.query.room_id
      io.on('connection',async function (socket){
        await app.db.models.findOneAndUpdate({email_id:req.body.email_id},{$set:{room_id:room}})
        //data is event which emits with chunk of video data
        //impliment 2 events in client side
        ss(socket).on('send_video',function(stream,data){
          //stream can be tranformed or verified by piping it into transform(if required)
          //we can record it if required
          stream.pipe(socket.broadcast.to(room).emit('recieve_video',stream))
          stream.on('end',()=>{
            console.log('connection slow...')
          })
        });

        socket.on('disconnect',function(){
          res.send('call ended')
      })
    })
    })


    app.post('/user',async(req,res) => {
      try{
       //we can use joi schema validation middlewares if needed
       
        let user = app.db.models.user(req.body)
        user.setPassword(req.body.password) 
        let newUser=await user.save();
        res.status(201).json({succces:true,payload:newUser})
      }catch(err){
        console.log(err)
        res.status(500).json({succces:false,error:err})
      }
    })

    app.post('/login',async(req,res) => {
      try{
         let user=await app.db.models.user.findOne({email:req.body.email})
        if(!user){
          res.status(400).json({succces:true,payload:[],message:'user not found'})
        }else {
          if (user.validPassword(req.body.password)){
            res.set('x-access-token',jwt.sign(user,config.secretKey,{ expiresIn: 60 * 60 },{algorithm: 'RS256'}))
            res.status(200).json({succces:true,payload:user})
          }else{
            res.status(403).json({succces:true,payload:[],message:'Incoorect password'})
          }
        }
      }catch(err){
        console.log(err)
        res.status(500).json({succces:false,error:err})
      }
    })

    //listen up
    
    http.listen(config.port, function(){
     console.log('server running on default port',3000)
    });
});

app.use (function (err, req, res, next){
  //Catch json error
  return res.json({"err":"invalid req JSON "});
});
